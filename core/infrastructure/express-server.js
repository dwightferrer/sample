const express = require('express')
// const config = require('../config')
require("dotenv").config();

const app = express()

app.get('/', (req, res) => {
    res.send('How are you world?')
})

app.listen(process.env.PORT, () =>
    console.log(`listening on port ${process.env.PORT}`),
);